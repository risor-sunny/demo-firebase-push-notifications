package com.example.fcm

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.Message
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.io.ByteArrayInputStream
import javax.annotation.PostConstruct

@RestController
@RequestMapping("/updates")
class Resource {

    companion object {
        private const val NOTIFICATION_TYPE = "notificationType"
        private const val CONVERSATION_ID = "conversationId"
        private const val MESSAGE_ID = "messageId"
    }

    private val firebaseConfig: String = "{\n" +
            "  \"type\": \"service_account\",\n" +
            "  \"project_id\": \"risor-559bf\",\n" +
            "  \"private_key_id\": \"ed9c8d68c40ba9591bdf32661b37239274bd6605\",\n" +
            "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDJv5Yq0hYHR/dw\\nMBhx7SIAQfk79tP2UbTdUeyMsh+QL+7+NbXp+syD0cn5SrbHP2TDWJeW+CqbyDtb\\nQnHtLLsJ9vongA9PffUY3QNoXTuPFpYAu/pZ3Gr97Qa8j50oAL2roHW9VyYc3r0U\\n1hy/n93gbPevvOnRLjlilWTL+oWJfehXHk7lM2sIDb6hIxy0XvKnKHJ6RGOBAfE1\\nXPprGyTfDYhonjg/TLnAIAjS/JjR37aM+pnbqLBioiRc/CFGeOaK3Us/bWtrI4Bn\\nwHgwqINn/cU4L0bMq/3i6pScxyf/Z5lRIvw6AglOVWQhm6ksROsiI4V8DA275L5P\\nNSWdsxR5AgMBAAECggEACZUOtBp13uMparvyEUkuzf3UMWesEh033rIzLsyQCLhq\\nHMteyE339EPc2LkWfNizN78Hti0z0uHcBd3eTOJMwPCo2YLw1zj3kPaAYK7a8xUi\\ny0aiYa3BLC3bnc2hbIK6f4IXzYYr4n+m0exIgcZ9UplCW2e44E1WfurzFmgf+xN4\\n/xIM47m1048DMcbKCxJwP0r2ZRjvkU0lGIQomeT9iRvVffQ7hNxHhm5ql4Uv+amj\\nWlD+41NLgM+OqBaKzbzO1/E2cy8uBMTNQDggrpfsASuoUNF2PgpYHSEfowbwFH6Q\\nzLM6dOR+6OYAjNPw+CHVHG2Ynf35c3eJkunrnow27QKBgQDtx76XxQebqXJ+3gKl\\nCjJXwyvPX5lGXA5nGtunnab0GXZs5rU/oAaXsqPHbZFAhFt9yLZW6VjWEJXObSML\\n2lHiW2BUoICzKgJLn16zVepugJ/jm0m8OLg58iv8zBmZi4+nXlfAabwco1bRdPGp\\nVZY8rpc9OUnPSt5UniFL4oLytQKBgQDZNQwehdXHvXH2kDMW8EROQ0SateTv7sG1\\ng6bWPfl4Vcre6YmdBF3Ow9JR+SURtcUcFp/SVI5Ue53jCEc3E+WrCOQqdmjeVj1a\\nHnqnyL5SAJTMvvFWjzdV3HPFjNJkaWMDbHLZ72fi48pAHflmiGIiiR6busNw7C8Z\\nUwiprwehNQKBgEYca3pcwk2HvRbjZ2w6gXWXOse+rHculaCZLsNRnPTEvUKHFFJa\\nYRdBuUEIcZfQbxm7oOgnjeYwpbtkOr039oUD5HePi+NIM70QlN/UKXfDvWFjXG5/\\nWvLbEVVM6Q95MzZqfv1hL/JRxzuje84uSkpCQJ3w0m6o6L2SksbJV0sxAoGBAJ1I\\nLF5tyz+3jq5b/KK/mLaTPzpIPF8BJIOrsUOx3NOmTCDeOyfX4ZW3drSlFJKebPzJ\\n5TEIyYeje9DKTcc0QTp4Vb/pvkZBbGu1ZlvYoPgAM7+PBHMJgahK2gpSzoanSWtR\\nPFlTYoaJLAsBqXb1ixo28FMUCjHa1MAhwEQe2geBAoGAfR+963yqOBw5uzzO12wi\\n/eDQBxX+H5N1s640QmB9hh05u7Gn2NpuqTfJII30i93Fbv8SskWmmh/ZEQ0/P3zB\\n2dnieLl1ll57PmjEo+4LSps00sJKGQzMjiWl8pOglvUVTx0g9purkJKaM/f7OcFP\\nZ0bLQqAt3B6GAjySEkbOvHw=\\n-----END PRIVATE KEY-----\\n\",\n" +
            "  \"client_email\": \"firebase-adminsdk-2tg7b@risor-559bf.iam.gserviceaccount.com\",\n" +
            "  \"client_id\": \"101826329846319342198\",\n" +
            "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
            "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
            "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
            "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-2tg7b%40risor-559bf.iam.gserviceaccount.com\"\n" +
            "}\n"


    @PostConstruct
    fun setup() {
        val options = FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(ByteArrayInputStream(firebaseConfig.toByteArray())))
                .build()
        FirebaseApp.initializeApp(options)
    }

    @GetMapping
    fun test(): ResponseEntity<String> {
        sendAndroidPushNotification("fSYmJ5cCTTWDFCuqGceyks:APA91bED-Ct_9vsp-WuWiJmhi2kYYdxO2qllXtrC2gipLWXVyYe7jaA1EvbvbPgbUnIa8KrvR5-H9qOQ0MaQpEn3xAUHu5lL4rHZtwPq24HuLN02v-_tE273edwsOUg4IH2Bx4e2m0Dd")
        return ResponseEntity("Hello", HttpStatus.OK)
    }

    private fun sendAndroidPushNotification(fcmToken: String) {
        val message = Message.builder()
                .putData(NOTIFICATION_TYPE, "USER_MESSAGE")
                .putData(CONVERSATION_ID, "5588375d-327d-4765-ab98-52ac17c6c9b3-8840d587-b50a-42de-9621-5e26cb451fda")
                .putData(MESSAGE_ID, "ce6670a4-ae5a-4061-a842-35d6ae6087c9")
                .setToken(fcmToken)
                .build()
        FirebaseMessaging.getInstance().send(message)
    }
}